/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Controller;
import model.BankAccount;

/**
 *
 * @author Nat
 */
public class View extends JFrame {
    
   
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;
   private static final double DEFAULT_RATE = 5;

     
   private Controller controller;
   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   
   
   public View(Controller controller)
   {  
      
       this.controller = controller;
      // Use instance variables for components 
      resultLabel = new JLabel("balance: ");

      // Use helper methods 
      createTextField();
      createButton();
      createPanel();
      
      setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }
   
   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   private void createButton()
   {
      button = new JButton("Add Interest");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            controller.addInterest(Double.parseDouble(rateField.getText()));
         }            
      }
      
      ActionListener listener = new AddInterestListener();
      button.addActionListener(listener);
   }
   
   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }
    
   public void updateInformation(BankAccount account){
      this.resultLabel.setText("Balance: "+account.getBalance());
   }
   
}
