/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;

import model.BankAccount;
import view.View;

/**
 *
 * @author Nat
 */
public class Controller {
    
    private BankAccount account;
    private View view;
    private static final double INITIAL_BALANCE = 1000;  
    
    public Controller(){
        account = new BankAccount(INITIAL_BALANCE);
        view = new View(this);
        update();
        
    }
    
    public void addInterest(double int_rate) {
        double rate = int_rate;
        double interest = account.getBalance() * rate / 100;
        account.deposit(interest);
        update();
        
    }
    
    public void update() {
        view.updateInformation(account);
    }
    
    public static void main(String[] args){
        Controller c = new Controller();
    }
    
}

